package id.co.sigma.minicoredb.service;

//AUTHOR : YURI LIADI & KHAIRIL HARIANTO
//DATE : 23/06/2019

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.sigma.minicoredb.model.Account;
import id.co.sigma.minicoredb.repository.AccountRepository;

@Service
public class AccountService {
	
	@Autowired
	private AccountRepository repository;
	
	public List<Account> findAll(){
		return repository.findAll();
	}
	
	public Account findById(Long id){
		return repository.findById(id).get();
	}
	
	public void save(Account account) {
		repository.save(account);
	}
	
	public void delete(Long id) {
		Account account = repository.findById(id).get();
		repository.delete(account);
	}
	
	public Account findByNumber(String number) {
		return repository.findByNumber(number);
	}

	
}
