package id.co.sigma.minicoredb.service;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import id.co.sigma.minicoredb.entity.Role;
import id.co.sigma.minicoredb.entity.Users;
import id.co.sigma.minicoredb.repository.RoleDao;
import id.co.sigma.minicoredb.repository.UserDao;



@Service("userService")
@CacheConfig(cacheNames = "userService")
public class UserService implements UserDetailsService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Cacheable(value = "thanos.user.findByUsernameAndPassword", unless = "#result == null")
    public Users findByUsernameAndPassword(String username, String password) {
        return userDao.findByUsernameAndPassword(username, password);
    }

    @Caching(evict = {
            @CacheEvict(value = "thanos.user.findByUsernameAndPassword", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "thanos.user.findByUsername", allEntries = true, beforeInvocation = true)})
    public void insertUser(Users user) {
        userDao.save(user);
    }

    @Caching(evict = {
            @CacheEvict(value = "thanos.user.findByUsernameAndPassword", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "thanos.user.findByUsername", allEntries = true, beforeInvocation = true)})
    public void updateUser(Users user) {
        userDao.save(user);
    }

    @Cacheable(value = "thanos.user.findByUsername", unless = "#result == null")
    public Users findByUsername(String username) {
        return userDao.findByUsername(username);
    }

    public Role findByAuthority(String roleName){
        return roleDao.findByAuthority(roleName);
    }

    
   // @Cacheable(value = "thanos.user.loadUserByUsername", unless = "#result == null")
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users user = userDao.findByUsername(username);
        if(user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new User(user.getUsername(),user.getPassword(),getAuthority());
    }

    public List<Users> findAll(){
        List<Users> list = userDao.findAll();
        return list;
    }

    private List getAuthority() {
        return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
    }
}
