package id.co.sigma.minicoredb.controller;

//AUTHOR : YURI LIADI & KHAIRIL HARIANTO
//DATE : 23/06/2019

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.sigma.minicoredb.model.Account;
import id.co.sigma.minicoredb.model.Customer;
import id.co.sigma.minicoredb.request.BalanceRequest;
import id.co.sigma.minicoredb.response.BalanceResponse;
import id.co.sigma.minicoredb.service.AccountService;
import id.co.sigma.minicoredb.service.CustomerService;

@RestController
public class AccountController {

	@Autowired
	private AccountService service;

	@Autowired
	private CustomerService custService;

	@RequestMapping(path = "account/get/all")
	public List<Account> getAll() {
		return service.findAll();
	}

	@RequestMapping(path = "account/get/{id}")
	public Account getById(@PathVariable("id") Long id) {
		return service.findById(id);
	}

	@RequestMapping(path = "account/delete/{id}")
	public List<Account> deleteById(@PathVariable("id") Long id) {
		service.delete(id);
		return getAll();
	}

	@RequestMapping(path = "account/balance")
	public BalanceResponse deleteById(@RequestBody BalanceRequest number) {
		String check = number.getNumber();
		Account account = service.findByNumber(check);
		BalanceResponse response = new BalanceResponse();

		response.setBalance(account.getBalance().toString());

		return response;
	}

	@RequestMapping(path = "account/inquiry/{id}/all")
	public List<Account> inquiryAll(@PathVariable Long idCustomer) {
		Customer customer = custService.findById(idCustomer);
		List<Account> acc = new ArrayList<Account>();
		acc.addAll(customer.getAccount());
		return acc;
	}
}
