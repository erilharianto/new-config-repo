package id.co.sigma.minicoredb.repository;

//AUTHOR : YURI LIADI & KHAIRIL HARIANTO
//DATE : 23/06/2019

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.sigma.minicoredb.model.Account;

public interface AccountRepository extends JpaRepository<Account, Long>{
	
	public Account findByNumber(String number);
}
