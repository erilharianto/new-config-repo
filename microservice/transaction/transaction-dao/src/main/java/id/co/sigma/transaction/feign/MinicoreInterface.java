package id.co.sigma.transaction.feign;
import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


import id.co.sigma.transaction.model.Customer;
import id.co.sigma.transaction.request.DepositRequest;
import id.co.sigma.transaction.request.TransferRequest;
import id.co.sigma.transaction.request.WithdrawalRequest;
import id.co.sigma.transaction.response.DepositResponse;
import id.co.sigma.transaction.response.MessageResponse;
import id.co.sigma.transaction.response.WithdrawalResponse;


@FeignClient("minicore")
public interface MinicoreInterface {
	
	@RequestMapping(path = "minicore/transaction/deposit")
	public DepositResponse deposit(@RequestBody DepositRequest request);
	
	@RequestMapping(path = "minicore/transaction/transfer")
	public ResponseEntity<Object> transfer(@RequestBody TransferRequest request);
	
	@RequestMapping(path = "minicore/transaction/withdrawal")
	public WithdrawalResponse withdrawal(@RequestBody WithdrawalRequest request);
	
	@RequestMapping(value = "/minicore/webservice/listUser", method = { RequestMethod.GET })
	List listUser();
}
