package id.co.sigma.hazelcasttask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HazelcastTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(HazelcastTaskApplication.class, args);
	}

}
