package id.co.sigma.crud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import id.co.sigma.crud.feign.MinicoreInterface;
import id.co.sigma.crud.model.Account;
import id.co.sigma.crud.response.MessageResponse;


@Service
public class AccountService {
	
//	@Autowired
//	private AccountRepository repository;
	
	@Autowired
	private MinicoreInterface interfaceMinicore;
	
	public List<Account> findAll(){
		return interfaceMinicore.getAllAccount();
	}
	
	public Account findById(Long id) {
		return interfaceMinicore.getByIdAccount(id);
	}
	
	public MessageResponse add(Account account) {
		return interfaceMinicore.addAccount(account);
	}
	
	public List<Account> deleteById(Long id){
		return interfaceMinicore.deleteByIdAccount(id);
	}
	
	public List<Account> inquiryAll(Long idCustomer){
		return interfaceMinicore.inquiryAll(idCustomer);
	}
	
//	public List<Account> findAll(){
//		return repository.findAll();
//	}
//	
//	public Account findById(Long id){
//		return repository.findById(id).get();
//	}
//	
//	public void save(Account account) {
//		repository.save(account);
//	}
//	
//	public void delete(Long id) {
//		Account account = repository.findById(id).get();
//		repository.delete(account);
//	}
	
}
