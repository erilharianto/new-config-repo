package id.co.sigma.crud.controller;

import java.util.ArrayList;
import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.co.sigma.crud.model.Customer;
import id.co.sigma.crud.response.MessageResponse;
import id.co.sigma.crud.service.CustomerService;

@RestController
public class CustomerController {

	@Autowired
	private CustomerService service;
	
	
	@RequestMapping(path = "customer/get/all")
	public List<Customer> findAll(){
		return service.findAll();
	}
	
	@RequestMapping(path = "customer/add")
	public MessageResponse add(@RequestBody Customer customer) {
		return service.add(customer);
	}
	
	@RequestMapping(path = "customer/update")
	public MessageResponse update(@RequestBody Customer customer) {
		return service.update(customer);
	}
	
	@RequestMapping(path = "customer/delete/{id}")
	public List<Customer> deleteById(@PathVariable("id") Long id){
		return service.deleteById(id);
	}
	
	@RequestMapping(path = "customer/get/{id}")
	public Customer getById(@PathVariable("id") Long id) {
		return service.getByIdCustomer(id);
	}
	
}
