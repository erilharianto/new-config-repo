package id.co.sigma.login.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.sigma.login.feign.MinicoreInterface;

@Service("userService")
public class UserService {
		
	@Autowired
	private MinicoreInterface interfaceMinicore;
	
	public List listUser() {
		return interfaceMinicore.listUser();
	}

}
