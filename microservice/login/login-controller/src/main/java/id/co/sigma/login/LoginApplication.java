package id.co.sigma.login;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import feign.RequestInterceptor;
import id.co.sigma.login.LoginApplication;
import id.co.sigma.login.configuration.FeignInterceptor;

@SpringBootApplication
@EnableAutoConfiguration
@EnableFeignClients
@ConfigurationProperties
@EnableDiscoveryClient
public class LoginApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoginApplication.class, args);
	}
	
	@Bean
	public RequestInterceptor getUserFeignClientInterceptor() {
		return new FeignInterceptor();
	}

}
